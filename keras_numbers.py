# -*- coding: utf-8 -*-
# !/usr/bin/python
import tensorflow as tf
import pickle
import numpy
import click
import keras
import pygame as pg
import os

import matplotlib.pylab as plt
import matplotlib.gridspec as gridspec

from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix

from pygame.locals import *


# TODO: Test other datasets like CIFAR-10


class App:

    def __init__(self):
        # os.environ['SDL_VIDEO_CENTERED'] = '1'  # chentered window position
        os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (100, 80)
        pg.init()
        self.w, self.h = 200, 200
        self.screen = pg.display.set_mode((self.w, self.h))
        self.screen.fill(pg.Color('white'))
        self.clock = pg.time.Clock()
        self.drawcolor = (0, 0, 0)
        icon = pg.image.load('icon.jpeg')
        icon = pg.transform.smoothscale(icon, (32, 32))
        pg.display.set_icon(icon)
        pg.display.set_caption('Mouse Input')

    def mainloop(self):
        while True:
            for event in pg.event.get():
                if event.type == pg.QUIT:
                    return []
                elif event.type == pg.MOUSEMOTION:
                    pos, rel = event.pos, event.rel
                    if event.buttons[0]:  # If the left mouse button is down.
                        # Draw a line from the pos to the previous pos.
                        pg.draw.line(self.screen, self.drawcolor, pos, (pos[0]-rel[0], pos[1]-rel[1]), 20)
                    elif event.buttons[2]:  # If the right mouse button is down.
                        # Erase by drawing a circle.
                        pg.draw.circle(self.screen, (255, 255, 255), pos, 25)
                elif event.type == KEYDOWN and event.key == K_RETURN:
                    surface = pg.display.get_surface()
                    surface_scaled = pg.transform.smoothscale(surface, (28, 28))
                    pg.image.save(surface_scaled, 'test.jpeg')
                    grey_array = []
                    for y in range(28):
                        for x in range(28):
                            pixel = surface_scaled.get_at((x, y))
                            grey = 0
                            for p in pixel:
                                grey += p
                            grey /= len(pixel)
                            if grey > 240:
                                grey = 255
                            elif grey < 100:
                                grey = 0
                            grey_array.append(int(grey))
                    return grey_array

            pg.display.flip()
            self.clock.tick(20)


def load_data(file, test_size, output_nodes):
    if os.path.exists('own_data.csv'):
        own_data = open('own_data.csv', 'r')
        own_data_list = own_data.readlines()
        own_data.close()
    else:
        own_data_list = []
    training_data_file = open(file, 'r')
    training_data_list = training_data_file.readlines()
    training_data_file.close()
    # from IPython import embed
    # embed()
    training_data_list.extend(own_data_list)
    inputs = list(numpy.arange(len(training_data_list)))
    targets = list(numpy.arange(len(training_data_list)))

    for numrec, record in enumerate(training_data_list):
        # split the record by the ',' commas
        all_values = record.split(',')
        # scale and shift the inputs
        inputs[numrec] = list((numpy.asfarray(all_values[1:]) / 255.0 * 0.99) + 0.01)
        # create the target output values (all 0.01, except the desired label which is 0.99)
        targets[numrec] = list(numpy.zeros(output_nodes) + 0.01)
        # all_values[0] is the target label for this record
        targets[numrec][int(all_values[0])] = 0.99
        targets[numrec][int(all_values[0])] = 0.99
    #     pass
    # pass

    x = numpy.array(inputs)

    # Specify the target labels and flatten the array
    y = numpy.array(targets)

    # del inputs, targets, training_data_list
    # Split the data up in train and test sets
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=test_size, shuffle=True)
    return x_train, x_test, y_train, y_test


def dnn(input_nodes, output_nodes, hidden_nodes, learning_rate):
    # Initialize the constructor
    model = keras.models.Sequential()
    # Add an input layer
    # init = keras.initializers.RandomNormal(mean=0.0, stddev=0.5, seed=None)
    model.add(keras.layers.Dense(hidden_nodes, activation='sigmoid', kernel_initializer='random_normal',
                                 bias_initializer='zeros', input_shape=(input_nodes,)))
    # Erstes Layer beinhaltet Input-Layer und erste hidden layer.

    # Add one hidden layer
    # model.add(Dense(hidden_nodes, kernel_initializer='random_normal',
    #  bias_initializer='zeros', activation='sigmoid'))
    # Add one hidden layer
    # model.add(Dense(hidden_nodes, kernel_initializer=init, activation='sigmoid'))
    # Add an output layer
    model.add(keras.layers.Dense(output_nodes, kernel_initializer='random_normal',
                                 bias_initializer='zeros', activation='sigmoid'))

    # Model output shape
    print(model.output_shape)

    # Model summary
    print(model.summary())

    sgd = keras.optimizers.SGD(lr=learning_rate, momentum=0.0)

    model.compile(optimizer=sgd,
                  loss='mean_squared_error',
                  metrics=['accuracy'])

    return model


def dnn_inverse(input_nodes, output_nodes, hidden_nodes):
    # Initialize the constructor
    model = keras.models.Sequential()

    def inverse_activation(x):
        # normalize
        tensor = tf.div(
            tf.subtract(
                x,
                tf.reduce_min(x)
            ),
            tf.subtract(
                tf.reduce_max(x),
                tf.reduce_min(x)
            )
        ) * 0.98 + 0.01
        ret = tf.log(tf.divide(tensor, tf.subtract(tf.ones(tf.shape(tensor)), tensor)))
        return ret

    # Add an input layer
    keras.utils.generic_utils.get_custom_objects().update(
        {'custom_activation': keras.layers.Activation(inverse_activation)})

    model.add(keras.layers.Dense(hidden_nodes, activation=inverse_activation, kernel_initializer='random_normal',
                                 bias_initializer='zeros', input_shape=(input_nodes,)))
    # Erstes Layer beinhaltet Input-Layer und erste hidden layer.

    model.add(keras.layers.Dense(output_nodes, kernel_initializer='random_normal',
                                 bias_initializer='zeros', activation=inverse_activation))

    return model


def train_dnn(model, x_train, y_train, batch_size, epochs):
    # All parameter gradients will be clipped to
    # a maximum norm of 1.

    # training stops if acc changes less than min_delta for patience numbers of epochs
    callb = keras.callbacks.EarlyStopping(monitor='acc', min_delta=0.001,
                                          patience=0, verbose=1, mode='auto')
    model.fit(x_train, y_train, epochs=epochs, batch_size=batch_size,
              verbose=1, callbacks=[callb])

    # verbose: Fortschrittsbalken ein oder ausschalten
    pickle.dump(model.get_weights(), open('ANN_Keras_Numbers.pkl', 'wb'))


def add_to_own_data(mouse_real, inp_tmp):
    if os.path.exists('own_data.csv'):
        old_own_data_file = open('own_data.csv', 'r')
        old_own_data = old_own_data_file.readlines()
        old_own_data_file.close()
    else:
        old_own_data = []
    tmp = [mouse_real]
    inp_tmp = numpy.asarray(inp_tmp, dtype=int)
    tmp.extend(inp_tmp)
    old_own_data.append(tmp)
    own_data_file = open('own_data.csv', 'w')
    for line in old_own_data:
        if line != '\n' and len(line) > 100:
            print(str(line).replace('[', '').replace(']', '').replace('\n', '').replace(' ', ''),
                  file=own_data_file)
    own_data_file.close()


def plot_confusion_matrix(y_true, y_pred):
    cm_array = confusion_matrix(y_true, y_pred)
    true_labels = numpy.unique(y_true)
    pred_labels = numpy.unique(y_pred)
    plt.imshow(cm_array, interpolation='nearest', cmap=plt.cm.jet)
    plt.title("Confusion matrix", fontsize=16)
    cbar = plt.colorbar(fraction=0.046, pad=0.04)
    cbar.set_label('Number of images', rotation=270, labelpad=30, fontsize=12)
    xtick_marks = numpy.arange(len(true_labels))
    ytick_marks = numpy.arange(len(pred_labels))
    plt.xticks(xtick_marks, true_labels, rotation=90)
    plt.yticks(ytick_marks, pred_labels)
    plt.tight_layout()
    plt.ylabel('True label', fontsize=14)
    plt.xlabel('Predicted label', fontsize=14)
    fig_size = plt.rcParams["figure.figsize"]
    fig_size[0] = 12
    fig_size[1] = 12
    plt.rcParams["figure.figsize"] = fig_size


@click.command()
@click.option('--input_data',
              default="training/mnist_complete.csv", type=str)
@click.option('--learning_rate', default=0.2, type=float)
@click.option('--batch_size', default=1, type=int)
# Gibt die Anzahl der Samples an, die gemeinsam (gemittelt) trainiert werden.
# Das kann den Einfluss von Ausreißern eindämmen.
@click.option('--test_size', default=0.2, type=float)
@click.option('--learn_new', is_flag=True)
@click.option('--input_nodes', default=784, type=int)
@click.option('--output_nodes', default=10, type=int)
@click.option('--hidden_nodes', default=100, type=int)
@click.option('--epochs', default=10, type=int)
@click.option('--mouse_input', is_flag=True)
@click.option('--weightings', default='ANN_Keras_Numbers.pkl', type=str)
def keraz_all(input_data=None, learning_rate=None, batch_size=None, test_size=None,
              learn_new=None, input_nodes=None, output_nodes=None, hidden_nodes=None,
              epochs=None, mouse_input=None, weightings=None):

    model = dnn(input_nodes, output_nodes, hidden_nodes, learning_rate)
    model_inv = dnn_inverse(output_nodes, input_nodes, hidden_nodes)
    x_train, x_test, y_train, y_test = load_data(input_data, test_size, output_nodes)
    if not learn_new and os.path.exists(weightings):
        model.set_weights(pickle.load(open(weightings, 'rb')))
    else:
        train_dnn(model, x_train, y_train, batch_size, epochs)
    info_file = open("pkl.info", 'w')
    info_file.write("hidden nodes:" + str(hidden_nodes) + '\n')
    info_file.close()
    print(model.evaluate(x_test, y_test, verbose=1))

    # Confusion_Matrix
    y_true, y_pred = y_test, model.predict(x_test)
    ytrue = []
    ypred = []
    for y in y_true:
        ytrue.append(y.argmax())
    for y in y_pred:
        ypred.append(y.argmax())
    plot_confusion_matrix(ytrue, ypred)

    # Plot Backqueries
    tmp = model.get_weights()
    tmp2 = [tmp[2].T, tmp[1], tmp[0].T, numpy.array([1] * 784)]
    model_inv.set_weights(tmp2)
    test_inv = []
    fig = plt.figure()
    gs = gridspec.GridSpec(2, 5)
    for i in range(10):
        default_array2 = [0.01] * 10
        default_array2[i] = 0.99
        test_inv.append(numpy.array(default_array2))
        input_test = numpy.array([test_inv[i]/tmp[3]])
        test = model_inv.predict(input_test)
        test = numpy.nan_to_num(test)
        if i < 5:
            ax = plt.subplot(gs[0, i])
        else:
            ax = plt.subplot(gs[1, i-5])
        ax.get_xaxis().set_ticks([])
        ax.get_yaxis().set_ticks([])
        ax.imshow(test.reshape(28, 28), cmap='Greys', interpolation='None')
        ax.set_title(str(i))
        fig.add_subplot(ax)
        plt.suptitle('Backquery', fontsize=16)
        plt.savefig('Backquery.png')
    plt.show()

    # Test with manual input data
    while mouse_input:
        app = App()
        inp_tmp = app.mainloop()
        pg.quit()
        if inp_tmp:
            inp_tmp_norm = list(1.0 - numpy.asfarray(inp_tmp) / numpy.asfarray(inp_tmp).max() * 0.99)
            result_tmp = model.predict(numpy.array([inp_tmp_norm]))
            result_sort = result_tmp.copy()
            result_sort.sort()
            if abs(result_sort[0][-1]-result_sort[0][-2]) < 0.1:
                print('Zahl konnte nicht eindeutlig bestimmt werden')
            else:
                print('geschriebene Ziffer lautet: ' + str(result_tmp[0].argmax()))
            print('Was haben Sie für eine Ziffer geschrieben?')
            try:
                mouse_real = int(input())
                if mouse_real < 10:
                    add_to_own_data(mouse_real, 255 - numpy.asfarray(inp_tmp))
                else:
                    print('wrong input (must be smaller than 10)')
            except ValueError as e:
                print('wrong input (', e, ')')
                pass
        print('Neuer Input (j/n)')
        mouse_input = input().lower().startswith('j')


if __name__ == '__main__':

    keraz_all()
