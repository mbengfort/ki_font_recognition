This script uses a `keras` Neuronal-Network to learn to recognize handwritten digits.
The basic learning uses the mnist database.
You can test the network and add own samples to the training-data via a mouse interface.

`keras_numbers_CNN.py` uses a Convolutional neural network.

Options:

* --input_data: training data; default="training/mnist_complete.csv"
* --learning_rate: default=0.2
*--batch_size: default=1 # Gibt die Anzahl der Samales an, die gemeinsam (gemittelt) trainiert werden. Das kann den Einfluss von Ausreißern eindämmen.
* --test_size: default=0.2
* --learn_new: ... or use old training results
* --input_nodes, default=784
* --output_nodes, default=10
* --hidden_nodes, default=100
* --epochs, default=10
* --mouse_input: enable mouse interface
* --weightings: saved pickle files with network weightings default='CNN_Keras_Numbers.pkl'

Automatically calculates Backquery to visualize the trained network.
![Backquery](BackqueryCNN.png)

![Confusion Matrix](Confusionmatrix.png)

Michael Bengfort (michael.bengfort@posteo.de)